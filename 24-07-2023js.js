const day = parseInt(prompt("Enter"));

switch(day) {
    case 1 : 
        console.log("Monday");
        break;
    case 2 :
        console.log("Tuesday");
        break;
    case 3 :
        console.log("Wednesday");
        break;    
    case 4 :
        console.log("Thursday");
        break;
    case 5 :
        console.log("Friday");
        break;
    case 6 :
        console.log("Saturday");
        break;
    case 7 :
        console.log("Sunday");
        break;elements
    default :
        console.log("Please enter correct information");
        break;
}

//Assignment
//map
//filter
//switch
//reduce

//By using higher order functions find the cube of each element in array--map
//list the elements which are odd
//product of elements of an array
//list the array elements which are more than given length-->filter

//using map
let elements = [1,2,3,4];
let cube = elements.map((elements) => elements*elements*elements);
console.log(cube);//1,8,9,64

//using filter
let elem1 = [1,2,3,4,5,6];
let oddNum = elem1.filter((elem1)=>elem1%2!=0);
console.log(oddNum);//1,3,5

//using reduce
let elem2 = [1,2,3,4];
let product = elem2.reduce((prod,elem2) =>prod*elem2);
console.log(product);//24

//Using filter
let arr = ["java","Html","Python","css"];
let result = arr.filter((arr) => arr.length >= 4);
console.log(result);//java,Html,Python
